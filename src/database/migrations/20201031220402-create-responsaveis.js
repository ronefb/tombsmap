'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('responsavel', { 
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
      },
      nome_organizacao: {
        type: Sequelize.STRING,
        allowNull: false
      },
      usu_id: {
        type: Sequelize.INTEGER,
        references: { model: 'usuario', key: 'id'},
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,

      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('responsables');
  }
};
