'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    await queryInterface.createTable('cemiterio_responsavel', { 
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
      },
      responsavel_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { model: 'responsavel', key: 'id'},
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      cemiterio_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { model: 'cemiterio', key: 'id'},
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      status:{
        type: Sequelize.INTEGER,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,

      }
    });
  },

  down: async (queryInterface, Sequelize) => {
   
     await queryInterface.dropTable('cemiterio_responsavel');
    
  }
};
