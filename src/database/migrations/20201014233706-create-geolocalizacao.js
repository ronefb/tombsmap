'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    
    await queryInterface.createTable('geolocalizacao', { 
       id: {
         type: Sequelize.INTEGER,
         primaryKey: true,
         autoIncrement: true,
         allowNull: false
       },
       geoLatitude: {
         type: Sequelize.DECIMAL(10,8),
         allowNull: false
       },
       geoLongitude: {
         type: Sequelize.DECIMAL(11,8),
         allowNull: false,
       },
	    placeId: {
		    type: Sequelize.INTEGER,
		    allowNull: false,
	    },
       created_at: {
         type: Sequelize.DATE,
         allowNull: false,
       },
       updated_at: {
         type: Sequelize.DATE,
         allowNull: false,
       }
     });
    
 },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('geolocalizations');
  }
};
