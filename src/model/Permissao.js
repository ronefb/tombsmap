const { Model, DataTypes } = require('sequelize');


class Permissao extends Model{
    static init(sequelize){
        super.init({
            nome: DataTypes.STRING,
            descricao: DataTypes.STRING
        },{
            sequelize,
            tableName: 'permissoes'
        })
    }

    static associate(models){
        this.belongsToMany(models.Usuario, { foreignKey:'permissoes_id', through: 'usuario_permissoes', as: 'usuarios' })
    }
}

    
module.exports = Permissao;