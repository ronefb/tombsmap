const { Model, DataTypes } = require('sequelize');


class Sepultura extends Model{
    static init(sequelize){
        super.init({    
            numero: DataTypes.INTEGER,
            referencia: DataTypes.STRING
        }, {
            sequelize,
            tableName: 'sepultura'
        })
    }

    static associate(models){
        this.hasOne(models.Geolocalizacao, { foreignKey:'geo_id', as: 'geolocalizacao' });
        this.hasOne(models.Cemiterio, { foreignKey:'cemiterio_id', as: 'cemiterio' });
        
    }
}

module.exports = Sepultura;