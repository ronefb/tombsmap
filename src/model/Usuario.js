const { Model, DataTypes } = require('sequelize');

class Usuario extends Model{
    static init(sequelize){
        super.init({    
            nome: DataTypes.STRING,
            email: DataTypes.STRING,
            senha: DataTypes.STRING
        }, {
            sequelize,
            tableName: 'usuario'
        })
    }

    static associate(models){
        this.belongsToMany(models.Permissao, { foreignKey:'usu_id', through: 'usuario_permissoes', as: 'permissao' })
    }
}

module.exports = Usuario;