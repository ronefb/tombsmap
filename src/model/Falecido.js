const { Model, DataTypes } = require('sequelize');


class Falecido extends Model{
    static init(sequelize){
        super.init({    
            nome: DataTypes.STRING,
            idade: DataTypes.INTEGER,
            data_nascimento: DataTypes.DATE,
            data_falecimento: DataTypes.DATE,
            local_sepultamento: DataTypes.STRING,
            local_velorio: DataTypes.STRING,
            cemiterio_id: DataTypes.INTEGER
        }, {
            sequelize,
            tableName: 'falecido'
        })
    }

    static associate(models){
        this.hasOne(models.Cemiterio, { foreignKey:'id', as: 'cemiterio' });
    }
   
}

module.exports = Falecido;