const express =  require("express");
const router = express.Router();
const ResponsavelService = require('../service/ResponsavelService');


router.get('/', ResponsavelService.index);
//router.get('/:id', ResponsavelService.show);
router.post('/', ResponsavelService.store);
//router.put('/permissao_id', ResponsavelService.update);
//router.delete('/permissao_id', ResponsavelService.destroy);
module.exports = router;

