const express =  require("express");
const router = express.Router();
const UsuarioService = require('../service/UsuarioService');
const authorize = require("../security/authMiddleware");

router.get('/teste', (req, res) => res.send('Ok'));
router.get('/', authorize(), UsuarioService.index);
router.get('/:usu_id', authorize(), UsuarioService.show);
router.post('/', UsuarioService.store);
router.post('/login', UsuarioService.login);
router.put('/:usu_id',authorize(), UsuarioService.update);
router.delete('/:usu_id',authorize(), UsuarioService.destroy);

module.exports = router;