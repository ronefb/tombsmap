const express =  require("express");
const router = express.Router();
const PermissaoService = require('../service/PermissaoService');


router.get('/', PermissaoService.index);
router.get('/:id', PermissaoService.show);
router.post('/:usu_id', PermissaoService.store);
//router.put('/permissao_id', PermissaoService.update);
router.delete('/:id', PermissaoService.destroy);
module.exports = router;

