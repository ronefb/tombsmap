
const Sepultura = require('../model/Sepultura');

module.exports = {

    async index(req, res){
        const sepultura = await Sepultura.findAll();
        return res.json(sepultura);
    },

    async store(req, res){
        const sepultura = await Sepultura.create(req.body);
        return res.json(sepultura);
    },

    async show (req, res){
        const sepultura = await Sepultura.findOne({where : {id: req.params.sepultura_id}})
        return res.json(sepultura)
    },

    async update(req, res){
        const sepultura = await Sepultura.update(req.body, {where : {id: req.params.sepultura_id}});
        const find = await Sepultura.findOne({where : {id: sepultura}});
        return res.json(find);
    },
    async destroy(req, res){
        await Sepultura.destroy({where: {id: req.params.sepultura_id}});
        return res.send();
    }
}