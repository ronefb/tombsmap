const Usuario = require('../model/Usuario');
const Cemiterio = require('../model/Cemiterio');
const Responsavel = require('../model/Responsavel');


module.exports = {

    async index(req, res){
        const cemiterio = await Cemiterio.findAll();
        return res.json(cemiterio);
    },

    async store(req, res){
        const { type, usu_id, cemiterio_id } = req.body;
        
        const user  = await Usuario.findByPk(usu_id);
        const cemiterio = await Cemiterio.findByPk(cemiterio_id);

        if(!user) return res.status(400).json({ error: 'Usuario não encontrado' })
        if(!cemiterio) return res.status(400).json({ error: 'Cemiterio não encontrado' })

        const responsavel = await Responsavel.create(req.body);

        await responsavel.addCemiterio(cemiterio);
       
        return res.json(responsavel);
    }
}