const Permissao = require('../model/Permissao');
const Usuario = require('../model/Usuario');

module.exports = {

    async index(req, res){
        const permissoes = await Permissao.findAll();
        return res.json(permissoes);
    },

    async store(req, res){
        /* Insere Uma Role em um usuario */
        const { usu_id } = req.params;
        const { nome, descricao } = req.body;
        
        const usuario  = await Usuario.findByPk(usu_id);

        if(!usuario){
            return res.status(400).json({ error: 'Usuario não encontrado' });
        }
        
        const [ permissao ] = await Permissao.findOrCreate({
            where: { nome },
            defaults: { descricao }
        })
        
        await usuario.addPermissao(permissao, { through: { status: 1 }});
        return res.json(permissao);
    },

    async show (req, res){
        const permissao = await Permissao.findOne({where : {id: req.params.id}})
        return res.json(permissao)
    },

    async destroy(req, res){
        await Permissoes.destroy({where: {id: req.params.id}});
        return res.send();
    }

}