const Cemiterio = require('../model/Cemiterio');
const Falecido = require('../model/Falecido');


module.exports = {

    async index(req, res){
       const falecidos = await Falecido.findAll();
       return res.json(falecidos);
    },
    async store(req, res){
        
        const cemiterio = Cemiterio.findOne({where: {id: req.params.cemiterio_id}, attributes: {exclude: ['cemiterio_id']}});
        if(!cemiterio) {
	        res.send("Cemitério não encontrado");
        	return res.status(400)
        }
        var nascimento = new Date(req.body.data_nascimento)
        var falecimento = new Date(req.body.data_falecimento)
        var idade = falecimento.getFullYear() - nascimento.getFullYear();
        if ( new Date(falecimento.getFullYear(), falecimento.getMonth(), falecimento.getDate()) < new Date(falecimento.getFullYear(), nascimento.getMonth(), nascimento.getDate()) )
            idade--;
        
        const cadastro = {...req.body, ...req.params, idade}
        const falecido = await Falecido.create(cadastro);
        return res.json(falecido)
    },
    async show (req, res){    
       
    },
    async update(req, res){
      
    },
    async destroy(req, res){
        
    }
}