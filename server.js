const express = require('express');
require('./src/database')
require('./src/config')
require("dotenv-safe").config();
var cors = require('cors')
const app =  express();
const bodyParser = require('body-parser')

app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())
app.use(cors())






//Require Controller
const ctrUsuario =  require("./src/controller/ControllerUsuario")
const ctrPermissao =  require("./src/controller/ControllerPermissao")
const ctrCemiterio =  require("./src/controller/ControllerCemiterio")
const ctrResponsavel = require("./src/controller/ControllerResponsavel");
const { route } = require('./src/controller/ControllerUsuario');
//User Controller
app.use('/api/usuario', ctrUsuario);
app.use('/api/permissao', ctrPermissao);
app.use('/api/cemiterio', ctrCemiterio);
app.use('/api/responsavel', ctrResponsavel);

app.listen(process.env.PORT || 3000);